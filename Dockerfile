FROM epenguincom/nitter
COPY ./entrypoint.sh /entrypoint.sh
COPY ./nitter.conf.pre /nitter.conf.pre
EXPOSE 8080
ENTRYPOINT ["/sbin/tini", "--", "/entrypoint.sh"]