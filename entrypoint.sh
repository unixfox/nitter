#!/bin/sh

set -e
set -u

NITTER_HOST=${NITTER_HOST:-localhost}
HTTPS=${HTTPS:-true}
TITLE=${TITLE:-localhost}
REDIS_HOST=${REDIS_HOST:-redis}
REDIS_PORT=${REDIS_PORT:-6379}
REDIS_CONNECTIONS=${REDIS_CONNECTIONS:-20}
REDIS_MAX_CONNECTIONS=${REDIS_MAX_CONNECTIONS:-30}
HMAC_KEY=${HMAC_KEY:-secretkey}
REPLACE_TWITTER=${REPLACE_TWITTER:-nitter.net}
REPLACE_YOUTUBE=${REPLACE_YOUTUBE:-invidio.us}
REPLACE_INSTAGRAM=${REPLACE_INSTAGRAM:-bibliogram.art}
PROXY_VIDEOS=${PROXY_VIDEOS:-true}
HLS_PLAYBACK=${HLS_PLAYBACK:-false}
INFINITE_SCROLL=${INFINITE_SCROLL:-false}

BUILD=/build
WORKD=/data

build_working_dir() {
    [ -d $WORKD ]             || mkdir -p $WOKRD

    [ -d $WORKD/tmp ]         || mkdir -p $WORKD/tmp
    [ -d $WORKD/public ]      || cp -rf $BUILD/public      $WORKD/.

    chown -R www-data:www-data $WORKD
    chmod 777 $WORKD
}

construct_nitter_conf() {
    [ -f $WORKD/nitter.conf ] && return

    cat /nitter.conf.pre > $WORKD/nitter.conf
    sed -i "s/HTTPS/$HTTPS/g; s/NITTER_HOST/$NITTER_HOST/g; s/TITLE/$TITLE/g;" $WORKD/nitter.conf
    sed -i "s/REDIS_HOST/$REDIS_HOST/g; s/REDIS_PORT/$REDIS_PORT/g; s/REDIS_PORT/$REDIS_PORT/g; s/REDIS_CONNECTIONS/$REDIS_CONNECTIONS/g; s/REDIS_MAX_CONNECTIONS/$REDIS_MAX_CONNECTIONS/g;" $WORKD/nitter.conf
    sed -i "s/HMAC_KEY/$HMAC_KEY/g;" $WORKD/nitter.conf
    sed -i "s/REPLACE_TWITTER/$REPLACE_TWITTER/g; s/REPLACE_YOUTUBE/$REPLACE_YOUTUBE/g; s/REPLACE_INSTAGRAM/$REPLACE_INSTAGRAM/g;" $WORKD/nitter.conf
    sed -i "s/PROXY_VIDEOS/$PROXY_VIDEOS/g; s/HLS_PLAYBACK/$HLS_PLAYBACK/g; s/INFINITE_SCROLL/$INFINITE_SCROLL/g;" $WORKD/nitter.conf
}

run_nitter_program() {
    cd $WORKD
    exec gosu www-data:www-data /usr/local/bin/nitter
}

# -- program starts

build_working_dir
construct_nitter_conf

if [[ $@ ]]; then 
    case "$1" in
	"init")
	    # workdir is prepared by now
	    ;;
	
	"nitter")
	    run_nitter_program;;
	
	*)
	    eval "exec $@";;
    esac
else
    run_nitter_program
fi

exit 0