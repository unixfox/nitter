[Server]
address = "0.0.0.0"
port = 8080
https = HTTPS  # disable to enable cookies when not using https
staticDir = "./public"
title = TITLE
hostname = NITTER_HOST

[Cache]
directory = "./tmp"
listMinutes = 240  # how long to cache list info (not the tweets, so keep it high)
rssMinutes = 10  # how long to cache rss queries
redisHost = REDIS_HOST
redisPort = REDIS_PORT
redisConnections = REDIS_CONNECTIONS # connection pool size
redisMaxConnections = REDIS_MAX_CONNECTIONS
# max, new connections are opened when none are available, but if the pool size
# goes above this, they're closed when released. don't worry about this unless
# you receive tons of requests per second

[Config]
hmacKey = HMAC_KEY # random key for cryptographic signing of video urls
base64Media = false # use base64 encoding for proxied media urls
tokenCount = 10
# minimum amount of usable tokens. tokens are used to authorize API requests,
# but they expire after ~1 hour, and have a limit of 187 requests.
# the limit gets reset every 15 minutes, and the pool is filled up so there's
# always at least $tokenCount usable tokens. again, only increase this if
# you receive major bursts all the time

# Change default preferences here, see src/prefs_impl.nim for a complete list
[Preferences]
theme = "Nitter"
replaceTwitter = REPLACE_TWITTER
replaceYouTube = REPLACE_YOUTUBE
replaceInstagram = REPLACE_INSTAGRAM
proxyVideos = PROXY_VIDEOS
hlsPlayback = HLS_PLAYBACK
infiniteScroll = INFINITE_SCROLL